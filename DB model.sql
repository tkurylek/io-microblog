SET FOREIGN_KEY_CHECKS=0;



DROP TABLE IF EXISTS Komentarz CASCADE
;
DROP TABLE IF EXISTS Rola CASCADE
;
DROP TABLE IF EXISTS Subskrypcja CASCADE
;
DROP TABLE IF EXISTS Tag CASCADE
;
DROP TABLE IF EXISTS Tag_Wiadomosc CASCADE
;
DROP TABLE IF EXISTS Uzytkownik CASCADE
;
DROP TABLE IF EXISTS Wiadomosc CASCADE
;
DROP TABLE IF EXISTS Zgloszenie CASCADE
;

CREATE TABLE KOMENTARZ
(
	ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
	autor INT NOT NULL,
	ID_WIADOMOSCI INT NOT NULL,
	tekst TEXT NOT NULL,
	PRIMARY KEY (ID),
	KEY (ID_WIADOMOSCI),
	KEY (autor)

) 
;


CREATE TABLE Rola
(
	ID SMALLINT NOT NULL,
	nazwa VARCHAR(15),
	PRIMARY KEY (ID)

) 
;


CREATE TABLE Subskrypcja
(
	ID_uzytkownik INT NOT NULL,
	ID_zrodlo INT NOT NULL,
	PRIMARY KEY (ID_uzytkownik, ID_zrodlo),
	KEY (ID_zrodlo),
	KEY (ID_uzytkownik)

) 
;


CREATE TABLE Tag
(
	ID INT NOT NULL,
	nazwa VARCHAR(70) NOT NULL,
	PRIMARY KEY (ID)

) 
;


CREATE TABLE Tag_Wiadomosc
(
	ID_tag INT NOT NULL,
	ID_wiadomosc INT NOT NULL,
	PRIMARY KEY (ID_tag, ID_wiadomosc),
	KEY (ID_wiadomosc),
	KEY (ID_tag)

) 
;


CREATE TABLE Uzytkownik
(
	ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
	email VARCHAR(80) NOT NULL,
	nazwisko VARCHAR(50) NOT NULL,
	imie VARCHAR(50) NOT NULL,
	nick VARCHAR(50) NOT NULL,
	wiek SMALLINT NOT NULL,
	opis TEXT,
	aktywne BOOL NOT NULL DEFAULT 0,
	rola SMALLINT NOT NULL,
	PRIMARY KEY (ID),
	KEY (rola)

) 
;


CREATE TABLE Wiadomosc
(
	ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
	tekst TEXT NOT NULL,
	autor INT NOT NULL,
	PRIMARY KEY (ID),
	KEY (autor)

) 
;


CREATE TABLE Zgloszenie
(
	ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
	ID_komentarza INT,
	ID_wiadomosci INT,
	komentarz TEXT NOT NULL,
	PRIMARY KEY (ID),
	KEY (ID_komentarza),
	KEY (ID_wiadomosci)

) 
;



SET FOREIGN_KEY_CHECKS=1;


ALTER TABLE Komentarz ADD CONSTRAINT FK_Komentarz_Wiadomosc 
	FOREIGN KEY (ID_WIADOMOSCI) REFERENCES Wiadomosc (ID)
	ON DELETE CASCADE ON UPDATE NO ACTION
;

ALTER TABLE Komentarz ADD CONSTRAINT FK_Komentarz_Uzytkownik 
	FOREIGN KEY (autor) REFERENCES Uzytkownik (ID)
	ON DELETE CASCADE ON UPDATE CASCADE
;

ALTER TABLE Subskrypcja ADD CONSTRAINT FK_Subskrypcja_Uzytkownik 
	FOREIGN KEY (ID_zrodlo) REFERENCES Uzytkownik (ID)
	ON DELETE CASCADE ON UPDATE CASCADE
;

ALTER TABLE Subskrypcja ADD CONSTRAINT FK_Subskrypcja_Uzytkownik_2 
	FOREIGN KEY (ID_uzytkownik) REFERENCES Uzytkownik (ID)
	ON DELETE CASCADE ON UPDATE CASCADE
;

ALTER TABLE Tag_Wiadomosc ADD CONSTRAINT FK_Tag_Wiadomosc_Wiadomosc 
	FOREIGN KEY (ID_wiadomosc) REFERENCES Wiadomosc (ID)
	ON DELETE CASCADE ON UPDATE CASCADE
;

ALTER TABLE Tag_Wiadomosc ADD CONSTRAINT FK_Tag_Wiadomosc_Tag 
	FOREIGN KEY (ID_tag) REFERENCES Tag (ID)
	ON DELETE CASCADE ON UPDATE CASCADE
;

ALTER TABLE Uzytkownik ADD CONSTRAINT FK_Uzytkownik_Rola 
	FOREIGN KEY (rola) REFERENCES Rola (ID)
	ON DELETE RESTRICT ON UPDATE CASCADE
;

ALTER TABLE Wiadomosc ADD CONSTRAINT FK_Wiadomosc_Uzytkownik 
	FOREIGN KEY (autor) REFERENCES Uzytkownik (ID)
	ON DELETE CASCADE ON UPDATE CASCADE
;

ALTER TABLE Zgloszenie ADD CONSTRAINT FK_Zgloszenie_Komentarz 
	FOREIGN KEY (ID_komentarza) REFERENCES Komentarz (ID)
	ON DELETE CASCADE ON UPDATE CASCADE
;

ALTER TABLE Zgloszenie ADD CONSTRAINT FK_Zgloszenie_Wiadomosc 
	FOREIGN KEY (ID_wiadomosci) REFERENCES Wiadomosc (ID)
	ON DELETE CASCADE ON UPDATE CASCADE
;
