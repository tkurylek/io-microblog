SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS rola;
DROP TABLE IF EXISTS komentarz;
DROP TABLE IF EXISTS subskrypcja;
DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS tag_wiadomosc;
DROP TABLE IF EXISTS uzytkownik;
DROP TABLE IF EXISTS wiadomosc;
DROP TABLE IF EXISTS zgloszenie;

SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE komentarz
(
  ID            INT NOT NULL AUTO_INCREMENT,
  autor         INT          NOT NULL,
  ID_WIADOMOSCI INT          NOT NULL,
  tekst         TEXT         NOT NULL,
  PRIMARY KEY (ID),
  KEY (ID_WIADOMOSCI),
  KEY (autor)

);

CREATE TABLE rola
(
  ID    INT NOT NULL AUTO_INCREMENT,
  nazwa VARCHAR(15),
  PRIMARY KEY (ID)
);

CREATE TABLE subskrypcja
(
  ID_uzytkownik INT NOT NULL,
  ID_zrodlo     INT NOT NULL,
  PRIMARY KEY (ID_uzytkownik, ID_zrodlo),
  KEY (ID_zrodlo),
  KEY (ID_uzytkownik)
);

CREATE TABLE tag
(
  ID    INT         NOT NULL,
  nazwa VARCHAR(70) NOT NULL,
  PRIMARY KEY (ID)
);

CREATE TABLE tag_wiadomosc
(
  ID_tag       INT NOT NULL,
  ID_wiadomosc INT NOT NULL,
  PRIMARY KEY (ID_tag, ID_wiadomosc),
  KEY (ID_wiadomosc),
  KEY (ID_tag)

);

CREATE TABLE uzytkownik
(
  ID       INT NOT NULL AUTO_INCREMENT,
  email    VARCHAR(80)  NOT NULL,
  nazwisko VARCHAR(50)  NOT NULL,
  imie     VARCHAR(50)  NOT NULL,
  nick     VARCHAR(50)  NOT NULL,
  wiek     SMALLINT     NOT NULL,
  opis     TEXT,
  aktywne  BOOL         NOT NULL DEFAULT 0,
  rola     INT     NOT NULL,
  PRIMARY KEY (ID),
  KEY (rola)

);

CREATE TABLE wiadomosc
(
  ID    INT NOT NULL AUTO_INCREMENT,
  tekst TEXT         NOT NULL,
  autor INT          NOT NULL,
  PRIMARY KEY (ID),
  KEY (autor)

);

CREATE TABLE zgloszenie
(
  ID            INT NOT NULL AUTO_INCREMENT,
  ID_komentarza INT,
  ID_wiadomosci INT,
  komentarz     TEXT         NOT NULL,
  PRIMARY KEY (ID),
  KEY (ID_komentarza),
  KEY (ID_wiadomosci)

);

ALTER TABLE komentarz ADD CONSTRAINT FK_Komentarz_Wiadomosc
FOREIGN KEY (ID_WIADOMOSCI) REFERENCES wiadomosc (ID)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

ALTER TABLE komentarz ADD CONSTRAINT FK_Komentarz_Uzytkownik
FOREIGN KEY (autor) REFERENCES uzytkownik (ID)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE subskrypcja ADD CONSTRAINT FK_Subskrypcja_Uzytkownik
FOREIGN KEY (ID_zrodlo) REFERENCES uzytkownik (ID)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE subskrypcja ADD CONSTRAINT FK_Subskrypcja_Uzytkownik_2
FOREIGN KEY (ID_uzytkownik) REFERENCES uzytkownik (ID)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE tag_wiadomosc ADD CONSTRAINT FK_Tag_Wiadomosc_Wiadomosc
FOREIGN KEY (ID_wiadomosc) REFERENCES wiadomosc (ID)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE tag_wiadomosc ADD CONSTRAINT FK_Tag_Wiadomosc_Tag
FOREIGN KEY (ID_tag) REFERENCES tag (ID)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE uzytkownik ADD CONSTRAINT FK_Uzytkownik_Rola
FOREIGN KEY (rola) REFERENCES rola (ID)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE wiadomosc ADD CONSTRAINT FK_Wiadomosc_Uzytkownik
FOREIGN KEY (autor) REFERENCES uzytkownik (ID)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE zgloszenie ADD CONSTRAINT FK_Zgloszenie_Komentarz
FOREIGN KEY (ID_komentarza) REFERENCES komentarz (ID)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE zgloszenie ADD CONSTRAINT FK_Zgloszenie_Wiadomosc
FOREIGN KEY (ID_wiadomosci) REFERENCES wiadomosc (ID)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

