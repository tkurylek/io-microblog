package pl.polsl.io.twitter.security;

import pl.polsl.io.twitter.domain.User;

public interface AuthenticatedUser {
	User getUser();
}
