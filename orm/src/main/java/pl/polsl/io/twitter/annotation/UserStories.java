package pl.polsl.io.twitter.annotation;

public @interface UserStories {
	UserStory[] value();
}
