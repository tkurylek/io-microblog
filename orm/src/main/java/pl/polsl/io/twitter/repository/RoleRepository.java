package pl.polsl.io.twitter.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.polsl.io.twitter.domain.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

	@Query("from Role where name like 'Administrator'")
	public Role findAdministrator();

	@Query("from Role where name like 'Uzytkownik'")
	public Role findUserRole();
}
