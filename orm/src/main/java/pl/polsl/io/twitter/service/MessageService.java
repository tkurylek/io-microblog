package pl.polsl.io.twitter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.polsl.io.twitter.domain.Message;
import pl.polsl.io.twitter.repository.MessageRepository;
import pl.polsl.io.twitter.security.AuthenticatedUser;
import pl.polsl.io.twitter.security.UnauthorisedException;

import javax.persistence.EntityNotFoundException;

@Service
public class MessageService {

	private AuthenticatedUser loggedUser;
	private MessageRepository messageRepository;

	@Autowired
	public MessageService(MessageRepository messageRepository) {
		this.messageRepository = messageRepository;
	}

	public Message addMessage(String messageContent) {
		throwExceptionWhenUserIsInactive();
		throwExceptionWhenMessageIsTooLong(messageContent);
		Message message = new Message();
		message.setText(messageContent);
		message.setAuthor(loggedUser.getUser());
		return messageRepository.save(message);
	}

	private void throwExceptionWhenMessageIsTooLong(String messageContent) {
		if(messageContent.length() > 120) {
			throw new IllegalArgumentException("The message should be shorter than 120 characters");
		}
	}

	public void removeMessage(Integer messageId) {
		throwExceptionWhenUserIsInactive();
		Message message = messageRepository.findOne(messageId);
		throwExceptionWhenMessageIsAlreadyDeleted(message);
		throwExceptionWhenUserHasNotAuthoredMessage(message);
		messageRepository.delete(messageId);
	}

	private void throwExceptionWhenUserIsInactive() {
		if (!loggedUser.getUser().getActive()) {
			throw new UnauthorisedException("User is inactive");
		}
	}

	private void throwExceptionWhenUserHasNotAuthoredMessage(Message message) {
		if (!message.getAuthor().equals(loggedUser.getUser())) {
			throw new UnauthorisedException("You are not eligible to delete this message");
		}
	}

	private void throwExceptionWhenMessageIsAlreadyDeleted(Message message) {
		if (message == null) {
			throw new EntityNotFoundException("Message is already deleted!");
		}
	}
}
