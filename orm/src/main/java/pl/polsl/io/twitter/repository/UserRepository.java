package pl.polsl.io.twitter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.polsl.io.twitter.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
