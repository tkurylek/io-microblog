package pl.polsl.io.twitter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.repository.UserRepository;

import javax.persistence.EntityNotFoundException;

@Service
public class SubscriptionService {

	private UserRepository userRepository;

	@Autowired
	public SubscriptionService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void subscribe(Long subscribingUserId, Long subscribedUserId) {
		throwExceptionWhenSubscribingToItself(subscribingUserId, subscribedUserId);
		User subscribingUser = userRepository.findOne(subscribingUserId);
		User subscribedUser = userRepository.findOne(subscribedUserId);
		throwExceptionWhenUserIsNotFound(subscribingUserId, subscribingUser);
		throwExceptionWhenUserIsNotFound(subscribedUserId, subscribedUser);
		subscribingUser.addToSubscribed(subscribedUser);
		subscribedUser.addToSubscribing(subscribingUser);
	}

	private void throwExceptionWhenSubscribingToItself(Long subscribingUserId, Long subscribedUserId) {
		if(subscribedUserId.equals(subscribingUserId)) {
			throw new IllegalArgumentException("You cannot subscribe to yourself");
		}
	}

	private void throwExceptionWhenUserIsNotFound(Long userId, User user) {
		if (user == null) {
			throw new EntityNotFoundException("Could not find user with id " + userId);
		}
	}
}
