package pl.polsl.io.twitter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.polsl.io.twitter.domain.Comment;
import pl.polsl.io.twitter.domain.Message;
import pl.polsl.io.twitter.repository.CommentRepository;
import pl.polsl.io.twitter.repository.MessageRepository;
import pl.polsl.io.twitter.security.AuthenticatedUser;
import pl.polsl.io.twitter.security.UnauthorisedException;

import javax.persistence.EntityNotFoundException;

@Service
public class CommentService {

	private MessageRepository messageRepository;
	private CommentRepository commentRepository;
	private AuthenticatedUser loggedUser;

	@Autowired
	public CommentService(MessageRepository messageRepository, CommentRepository commentRepository) {
		this.messageRepository = messageRepository;
		this.commentRepository = commentRepository;
	}

	public Comment addComment(String commentContent, Integer messageId) {
		Message message = messageRepository.findOne(messageId);
		throwExceptionWhenMessageIsNotFound(message);
		throwExceptionWhenUserIsNotLoggedIn();
		throwExceptionWhenUserIsInactive();
		Comment comment = new Comment();
		comment.setText(commentContent);
		comment.setAuthor(loggedUser.getUser());
		comment.setMessage(message);
		commentRepository.save(comment);
		message.addComment(comment);
		messageRepository.save(message);
		return comment;
	}

	private void throwExceptionWhenUserIsNotLoggedIn() {
		if (loggedUser.getUser() == null) {
			throw new UnauthorisedException("User is not logged in");
		}
	}

	private void throwExceptionWhenUserIsInactive() {
		if (!loggedUser.getUser().getActive()) {
			throw new UnauthorisedException("User is inactive");
		}
	}

	private void throwExceptionWhenMessageIsNotFound(Message message) {
		if (message == null) {
			throw new EntityNotFoundException("Cannot add comment to non existing message");
		}
	}
}
