package pl.polsl.io.twitter.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "zgloszenie")
public class Report {

	@Id
	@GeneratedValue
	private Integer id;

	@Column(name = "komentarz")
	private String text;

	@ManyToOne
	@JoinColumn(name = "ID_komentarza", referencedColumnName = "ID")
	private Comment comment;

	@ManyToOne
	@JoinColumn(name = "ID_wiadomosci", referencedColumnName = "ID")
	private Message message;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getText() {
		return text;
	}

	public void setText(String comment) {
		this.text = comment;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Report report = (Report) o;

		if (text != null ? !text.equals(report.text) : report.text != null)
			return false;
		if (id != null ? !id.equals(report.id) : report.id != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (text != null ? text.hashCode() : 0);
		return result;
	}
}
