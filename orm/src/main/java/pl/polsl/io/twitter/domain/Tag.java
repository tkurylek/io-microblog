package pl.polsl.io.twitter.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

@Entity
public class Tag {

	@Id
	@GeneratedValue
	private Integer id;

	@Column(name = "nazwa")
	private String name;

	@ManyToMany(cascade = ALL)
	@JoinTable(name = "tag_wiadomosc",
			joinColumns = @JoinColumn(name = "ID"),
			inverseJoinColumns = @JoinColumn(name = "ID_tag"))
	private Set<Message> messages;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Tag tag = (Tag) o;

		if (id != null ? !id.equals(tag.id) : tag.id != null)
			return false;
		if (name != null ? !name.equals(tag.name) : tag.name != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		return result;
	}
}
