package pl.polsl.io.twitter.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "komentarz")
public class Comment {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "tekst")
	private String text;

	@OneToMany(mappedBy = "comment")
	private Collection<Report> reports;

	@ManyToOne
	@JoinColumn(name = "autor", referencedColumnName = "ID", nullable = false)
	private User author;

	@ManyToOne(fetch = LAZY)
	@JoinColumn(name = "ID_WIADOMOSCI", referencedColumnName = "ID", nullable = false)
	private Message message;

	public Comment() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Collection<Report> getReports() {
		return reports;
	}

	public void setReports(Collection<Report> reports) {
		this.reports = reports;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Comment comment = (Comment) o;

		if (id != null ? !id.equals(comment.id) : comment.id != null)
			return false;
		if (text != null ? !text.equals(comment.text) : comment.text != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (text != null ? text.hashCode() : 0);
		return result;
	}
}
