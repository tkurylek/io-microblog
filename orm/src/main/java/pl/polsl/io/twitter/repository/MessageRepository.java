package pl.polsl.io.twitter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.polsl.io.twitter.domain.Message;

@Repository
public interface MessageRepository extends CrudRepository<Message, Integer> {
}
