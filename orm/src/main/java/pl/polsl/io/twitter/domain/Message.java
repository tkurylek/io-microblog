package pl.polsl.io.twitter.domain;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.REMOVE;

@Entity
@Table(name = "wiadomosc")
public class Message {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	@Basic
	@Column(name = "tekst")
	private String text;

	@ManyToOne
	@JoinColumn(name = "autor", referencedColumnName = "ID", nullable = false)
	private User author;

	@OneToMany(mappedBy = "message")
	private Set<Report> reports = new HashSet<>();

	@OneToMany(mappedBy = "message", cascade = REMOVE)
	private Set<Comment> comments = new HashSet<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Set<Report> getReports() {
		return reports;
	}

	public void setReports(Set<Report> reports) {
		this.reports = reports;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public void addComment(Comment comment) {
		comments.add(comment);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Message message = (Message) o;

		if (id != null ? !id.equals(message.id) : message.id != null)
			return false;
		if (text != null ? !text.equals(message.text) : message.text != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (text != null ? text.hashCode() : 0);
		return result;
	}
}
