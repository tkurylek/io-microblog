package pl.polsl.io.twitter.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Uzytkownik", schema = "", catalog = "twitter")
public class User {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Long id;

	@Column(name = "email")
	private String email;

	@Column(name = "nazwisko")
	private String lastName;

	@Column(name = "imie")
	private String firstName;

	@Column(name = "nick")
	private String nickName;

	@Column(name = "wiek")
	private Integer age;

	@Column(name = "opis")
	private String description;

	@Column(name = "aktywne")
	private Boolean active;


	@ManyToOne
	@JoinColumn(name = "rola", referencedColumnName = "ID", nullable = false)
	private Role role;

	@OneToMany(mappedBy = "author")
	private Set<Message> messages;

	@OneToMany(mappedBy = "author")
	private Set<Comment> comments;

	@ManyToMany
	@JoinTable(name = "subskrypcja",
			joinColumns = @JoinColumn(name = "ID"),
			inverseJoinColumns = @JoinColumn(name = "ID_uzytkownik"))
	private Set<User> subscribedUsers = new HashSet<>();

	@ManyToMany
	@JoinTable(name = "subskrypcja",
			joinColumns = @JoinColumn(name = "ID"),
			inverseJoinColumns = @JoinColumn(name = "ID_zrodlo"))
	private Set<User> subscribingUsers = new HashSet<>();

	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Set<Message> getMessages() {
		return messages;
	}

	public void setMessages(Set<Message> message) {
		this.messages = message;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<User> getSubscriptions() {
		return subscribedUsers;
	}

	public Set<User> getSubscribingUsers() {
		return subscribingUsers;
	}

	public void addToSubscribed(User subscribedUser) {
		subscribedUsers.add(subscribedUser);
	}

	public void addToSubscribing(User subscribingUser) {
		subscribingUsers.add(subscribingUser);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		User user = (User) o;

		if (active != null ? !active.equals(user.active) : user.active != null)
			return false;
		if (age != null ? !age.equals(user.age) : user.age != null)
			return false;
		if (description != null ? !description.equals(user.description) : user.description != null)
			return false;
		if (email != null ? !email.equals(user.email) : user.email != null)
			return false;
		if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null)
			return false;
		if (id != null ? !id.equals(user.id) : user.id != null)
			return false;
		if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null)
			return false;
		if (nickName != null ? !nickName.equals(user.nickName) : user.nickName != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
		result = 31 * result + (nickName != null ? nickName.hashCode() : 0);
		result = 31 * result + (age != null ? age.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (active != null ? active.hashCode() : 0);
		return result;
	}
}
