package pl.polsl.io.twitter.security;

public class UnauthorisedException extends RuntimeException {

	public UnauthorisedException(String message) {
		super(message);
	}
}
