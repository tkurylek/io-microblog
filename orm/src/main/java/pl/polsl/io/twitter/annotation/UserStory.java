package pl.polsl.io.twitter.annotation;

import java.lang.annotation.Repeatable;

@Repeatable(value = UserStories.class)
public @interface UserStory {
	String value();
}
