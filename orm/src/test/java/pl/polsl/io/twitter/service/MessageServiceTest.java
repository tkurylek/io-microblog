package pl.polsl.io.twitter.service;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import pl.polsl.io.twitter.annotation.UserStory;
import pl.polsl.io.twitter.domain.Comment;
import pl.polsl.io.twitter.domain.Message;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.repository.CommentRepository;
import pl.polsl.io.twitter.repository.MessageRepository;
import pl.polsl.io.twitter.repository.RoleRepository;
import pl.polsl.io.twitter.repository.UserRepository;
import pl.polsl.io.twitter.security.AuthenticatedUser;
import pl.polsl.io.twitter.security.UnauthorisedException;
import pl.polsl.io.twitter.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static pl.polsl.io.twitter.test.MessageBuilder.aMessage;
import static pl.polsl.io.twitter.test.UserBuilder.aUser;

public class MessageServiceTest extends IntegrationTest {

	private static final String TOO_LONG_MESSAGE = "Lorem ipsum dolor sit amet tellus. Donec eu condimentum magna " +
			"felis" + " enim, ac augue. Sed sagittis libero. Curabitur tempor.";
	private static final String SAMPLE_MESSAGE = "Sample message";
	@Autowired
	CommentRepository commentRepository;

	@Mock
	AuthenticatedUser mockedAuthenticatedUser;

	@Autowired
	@InjectMocks
	CommentService commentService;

	@Autowired
	@InjectMocks
	MessageService messageService;

	@Test
	@UserStory("As a user I can add new messages")
	public void shouldAddMessage() {
		// given
		User author = aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository
				(userRepository);
		given(mockedAuthenticatedUser.getUser()).willReturn(author);

		// when
		Message result = messageService.addMessage(SAMPLE_MESSAGE);

		// then
		assertThat(messageRepository.findOne(result.getId())).isNotNull();
	}

	@Test(expected = UnauthorisedException.class)
	@UserStory("As an inactive user I cannot add new messages")
	public void shouldNotAddMessageBecauseAuthorIsInactive() {
		// given
		User author = aUser().withMinimalData().withRole(roleRepository.findUserRole()).inactive().savedInRepository
				(userRepository);
		given(mockedAuthenticatedUser.getUser()).willReturn(author);

		// when
		messageService.addMessage(SAMPLE_MESSAGE);

		// then
		// ... exception is thrown
	}

	@Test(expected = IllegalArgumentException.class)
	@UserStory("As a user I cannot add messages longer than 120 characters")
	public void shouldNotAddTooLongMessage() {
		// given
		given(mockedAuthenticatedUser.getUser()).willReturn(aUser().withMinimalData().withRole(roleRepository
				.findUserRole()).savedInRepository(userRepository));

		// when
		messageService.addMessage(TOO_LONG_MESSAGE);

		// then
		// ... exception is thrown
	}

	@Test
	public void shouldDeleteMessageWithComments() {
		// given
		User loggedUser = aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository
				(userRepository);
		given(mockedAuthenticatedUser.getUser()).willReturn(loggedUser);
		Message message = aMessage().withMinimalData().withAuthor(loggedUser).savedInRepository(messageRepository);
		Comment comment = commentService.addComment("Sample comment", message.getId());

		// when
		messageService.removeMessage(message.getId());

		// then
		assertThat(messageRepository.findOne(message.getId())).isNull();
		assertThat(commentRepository.findOne(comment.getId())).isNull();
	}

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	MessageRepository messageRepository;
}