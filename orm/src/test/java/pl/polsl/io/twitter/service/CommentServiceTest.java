package pl.polsl.io.twitter.service;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import pl.polsl.io.twitter.annotation.UserStory;
import pl.polsl.io.twitter.domain.Comment;
import pl.polsl.io.twitter.domain.Message;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.repository.CommentRepository;
import pl.polsl.io.twitter.repository.MessageRepository;
import pl.polsl.io.twitter.repository.RoleRepository;
import pl.polsl.io.twitter.repository.UserRepository;
import pl.polsl.io.twitter.security.AuthenticatedUser;
import pl.polsl.io.twitter.security.UnauthorisedException;
import pl.polsl.io.twitter.test.IntegrationTest;

import javax.persistence.EntityNotFoundException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static pl.polsl.io.twitter.test.MessageBuilder.aMessage;
import static pl.polsl.io.twitter.test.UserBuilder.aUser;

public class CommentServiceTest extends IntegrationTest {

	private static final String COMMENT_CONTENT = "Just a test comment";
	private static final Integer NON_EXISTING_MESSAGE_ID = -1;
	@Autowired
	CommentRepository commentRepository;

	@Mock
	AuthenticatedUser mockedAuthenticatedUser;

	@Autowired
	@InjectMocks
	CommentService commentService;

	@Test
	@UserStory("As a user I can add comments to messages")
	public void shouldAddComment() {
		// given
		User author = aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository
				(userRepository);
		Message message = aMessage().withMinimalData().withAuthor(author).savedInRepository(messageRepository);
		given(mockedAuthenticatedUser.getUser()).willReturn(author);

		// when
		Comment comment = commentService.addComment(COMMENT_CONTENT, message.getId());

		// then
		assertThat(comment.getMessage()).isEqualTo(message);
		assertThat(message.getComments()).contains(comment);
	}

	@Test(expected = UnauthorisedException.class)
	@UserStory("As an inactive user I cannot add comments to messages")
	public void shouldNotAddCommentBecauseAuthorIsInactive() {
		// given
		User inactiveAuthor = aUser().withMinimalData().withRole(roleRepository.findUserRole()).inactive()
				.savedInRepository(userRepository);
		Message message = aMessage().withMinimalData().withAuthor(inactiveAuthor).savedInRepository(messageRepository);
		given(mockedAuthenticatedUser.getUser()).willReturn(inactiveAuthor);

		// when
		commentService.addComment(COMMENT_CONTENT, message.getId());

		// then
		// ... an exception is thrown
	}

	@Test(expected = EntityNotFoundException.class)
	@UserStory("As a user I cannot add comments to non existing messages")
	public void shouldNotAddCommentToNonExistingMessage() {
		// given
		User author = aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository
				(userRepository);
		given(mockedAuthenticatedUser.getUser()).willReturn(author);

		// when
		commentService.addComment(COMMENT_CONTENT, NON_EXISTING_MESSAGE_ID);

		// then
		// ... an exception is thrown
	}

	@Test(expected = UnauthorisedException.class)
	@UserStory("As a not logged in user I cannot add comments to messages")
	public void shouldNotAddCommentToWhenNotLoggedIn() {
		// given
		User inactiveAuthor = aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository
				(userRepository);
		Message message = aMessage().withMinimalData().withAuthor(inactiveAuthor).savedInRepository(messageRepository);

		// when
		commentService.addComment(COMMENT_CONTENT, message.getId());

		// then
		// ... an exception is thrown
	}

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	MessageRepository messageRepository;
}