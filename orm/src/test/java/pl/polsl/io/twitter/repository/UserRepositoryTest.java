package pl.polsl.io.twitter.repository;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.polsl.io.twitter.test.UserBuilder.aUser;

public class UserRepositoryTest extends IntegrationTest {


	@Autowired
	UserRepository userRepository;

	@Test
	public void shouldCreateUser() {
		// given
		User user = aUser().withMinimalData().withRole(roleRepository.findUserRole()).build();

		// when
		userRepository.save(user);

		// then
		assertThat(userRepository.findAll()).contains(user);
	}

	@Test
	public void shouldDeleteUser() {
		// given
		User user = aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository(userRepository);

		// when
		userRepository.delete(user);

		// then
		assertThat(userRepository.findAll()).doesNotContain(user);
	}

	@Autowired
	RoleRepository roleRepository;
}