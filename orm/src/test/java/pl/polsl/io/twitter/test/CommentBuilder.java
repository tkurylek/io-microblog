package pl.polsl.io.twitter.test;

import pl.jsolve.sweetener.builder.Builder;
import pl.polsl.io.twitter.domain.Comment;
import pl.polsl.io.twitter.domain.Message;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.repository.CommentRepository;

public class CommentBuilder extends Builder<Comment> {

	public static CommentBuilder aComment() {
		return new CommentBuilder();
	}

	public CommentBuilder withAuthor(User author) {
		getBuiltObject().setAuthor(author);
		return this;
	}

	public CommentBuilder withMessage(Message message) {
		getBuiltObject().setMessage(message);
		return this;
	}

	public CommentBuilder withText(String text) {
		getBuiltObject().setText(text);
		return this;
	}

	public Comment savedInRepository(CommentRepository commentRepository) {
		return commentRepository.save(getBuiltObject());
	}
}
