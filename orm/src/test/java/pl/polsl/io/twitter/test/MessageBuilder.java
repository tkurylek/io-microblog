package pl.polsl.io.twitter.test;

import pl.jsolve.sweetener.builder.Builder;
import pl.polsl.io.twitter.domain.Comment;
import pl.polsl.io.twitter.domain.Message;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.repository.MessageRepository;

import static com.google.common.collect.Sets.newHashSet;

public class MessageBuilder extends Builder<Message> {

	public static MessageBuilder aMessage() {
		return new MessageBuilder();
	}

	public MessageBuilder withMinimalData() {
		return this.withText("Sample message");
	}

	public MessageBuilder withText(String text) {
		getBuiltObject().setText(text);
		return this;
	}

	public MessageBuilder withAuthor(User author) {
		getBuiltObject().setAuthor(author);
		return this;
	}

	public MessageBuilder withComments(Comment ... comments) {
		getBuiltObject().setComments(newHashSet(comments));
		return this;
	}

	public Message savedInRepository(MessageRepository messageRepository) {
		return messageRepository.save(getBuiltObject());
	}
}
