package pl.polsl.io.twitter.test;

import org.springframework.stereotype.Component;
import pl.jsolve.sweetener.builder.Builder;
import pl.polsl.io.twitter.domain.Role;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.repository.UserRepository;

public class UserBuilder extends Builder<User> {

	public static UserBuilder aUser() {
		return new UserBuilder();
	}

	public UserBuilder withMinimalData() {
		return this.withFirstName("Adam").withLastName("Bien").withNickName("abien").withEmail("adam@bien.com")
				.withAge(25).active();
	}

	public UserBuilder withEmail(String email) {
		getBuiltObject().setEmail(email);
		return this;
	}

	public UserBuilder withLastName(String lastName) {
		getBuiltObject().setLastName(lastName);
		return this;
	}

	public UserBuilder withFirstName(String firstName) {
		getBuiltObject().setFirstName(firstName);
		return this;
	}

	public UserBuilder withNickName(String nickName) {
		getBuiltObject().setNickName(nickName);
		return this;
	}

	public UserBuilder withAge(Integer age) {
		getBuiltObject().setAge(age);
		return this;
	}

	public UserBuilder withDescription(String description) {
		getBuiltObject().setDescription(description);
		return this;
	}

	public UserBuilder withActive(Boolean active) {
		getBuiltObject().setActive(active);
		return this;
	}

	public UserBuilder active() {
		getBuiltObject().setActive(true);
		return this;
	}

	public UserBuilder inactive() {
		getBuiltObject().setActive(false);
		return this;
	}

	public UserBuilder withRole(Role role) {
		getBuiltObject().setRole(role);
		return this;
	}

	public User savedInRepository(UserRepository userRepository) {
		return userRepository.save(getBuiltObject());
	}
}
