package pl.polsl.io.twitter.repository;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.polsl.io.twitter.domain.Message;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.polsl.io.twitter.test.MessageBuilder.aMessage;
import static pl.polsl.io.twitter.test.UserBuilder.aUser;

public class MessageRepositoryTest extends IntegrationTest {

	private static final String SAMPLE_MESSAGE = "Sample message";
	@Autowired
	MessageRepository messageRepository;

	@Test
	public void shouldCreateMessage() {
		// given

		Message message = aMessage().withText(SAMPLE_MESSAGE).withAuthor(anAuthor()).build();

		// when
		messageRepository.save(message);

		// then
		assertThat(messageRepository.findAll()).contains(message);
	}

	@Test
	public void shouldDeleteMessage() {
		// given
		Message message = aMessage().withText(SAMPLE_MESSAGE).withAuthor(anAuthor()).savedInRepository
				(messageRepository);

		// when
		messageRepository.delete(message);

		// then
		assertThat(messageRepository.findAll()).doesNotContain(message);
	}

	private User anAuthor() {
		return aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository(userRepository);
	}

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;
}