package pl.polsl.io.twitter.repository;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.polsl.io.twitter.domain.Comment;
import pl.polsl.io.twitter.domain.Message;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.polsl.io.twitter.test.CommentBuilder.aComment;
import static pl.polsl.io.twitter.test.MessageBuilder.aMessage;
import static pl.polsl.io.twitter.test.UserBuilder.aUser;

public class CommentRepositoryTest extends IntegrationTest {

	private static final String TEST_COMMENT_TEXT = "Just a comment";
	private static final String ANOTHER_TEST_COMMENT_TEXT = "Just another comment";

	@Autowired
	CommentRepository commentRepository;

	@Test
	public void shouldCreateComment() {
		// given
		Comment comment = aComment().withText(TEST_COMMENT_TEXT).withMessage(anExistingMessage()).withAuthor(anAuthor
				()).build();

		// when
		commentRepository.save(comment);

		// then
		assertThat(commentRepository.findAll()).contains(comment);
	}


	@Test
	public void shouldDeleteComment() {
		// given
		Comment comment = aComment().withText(TEST_COMMENT_TEXT).withMessage(anExistingMessage()).withAuthor(anAuthor
				()).savedInRepository(commentRepository);

		// when
		commentRepository.delete(comment);

		// then
		assertThat(commentRepository.findAll()).doesNotContain(comment);
	}

	@Test
	public void shouldFindAllComments() {
		// given
		Comment comment = aComment().withText(TEST_COMMENT_TEXT).withMessage(anExistingMessage()).withAuthor(anAuthor
				()).savedInRepository(commentRepository);
		Comment anotherComment = aComment().withText(ANOTHER_TEST_COMMENT_TEXT).withMessage(anExistingMessage())
				.withAuthor(anAuthor()).savedInRepository(commentRepository);

		// when
		Iterable<Comment> result = commentRepository.findAll();

		// then
		assertThat(result).containsExactly(comment, anotherComment);
	}

	private Message anExistingMessage() {
		return aMessage().withMinimalData().withAuthor(anAuthor()).savedInRepository(messageRepository);
	}

	private User anAuthor() {
		return aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository(userRepository);
	}

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	MessageRepository messageRepository;
}