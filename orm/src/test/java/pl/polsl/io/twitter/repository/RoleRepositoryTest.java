package pl.polsl.io.twitter.repository;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.polsl.io.twitter.domain.Role;
import pl.polsl.io.twitter.test.IntegrationTest;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;

public class RoleRepositoryTest extends IntegrationTest {

	private static final String ADMINISTRATOR = "Administrator";
	private static final String USER = "Uzytkownik";
	@Autowired
	RoleRepository roleRepository;

	@Test
	public void shouldSaveRole() {
		// given
		Role role = new Role(ADMINISTRATOR);

		// when
		roleRepository.save(role);

		// then
		assertThat(roleRepository.findAll()).contains(role);
	}

	@Test
	public void shouldDeleteRole() {
		// given
		Role role = new Role(ADMINISTRATOR);
		roleRepository.save(role);

		// when
		roleRepository.delete(role);

		// then
		assertThat(roleRepository.findAll()).doesNotContain(role);
	}

	@Test
	public void shouldFindAll() {
		// given
		List<Role> roles = newArrayList(new Role(ADMINISTRATOR), new Role(USER));
		roleRepository.save(roles);

		// when
		Iterable<Role> result = roleRepository.findAll();

		// then
		assertThat(result).containsAll(roles);
	}

	@Test
	public void shouldFindAdministrator() {
		// when
		Role result = roleRepository.findAdministrator();

		// then
		assertThat(result).isNotNull();
	}

	@Test
	public void shouldFindUser() {
		// when
		Role result = roleRepository.findUserRole();

		// then
		assertThat(result).isNotNull();
	}
}