package pl.polsl.io.twitter.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.polsl.io.twitter.annotation.UserStory;
import pl.polsl.io.twitter.domain.User;
import pl.polsl.io.twitter.repository.RoleRepository;
import pl.polsl.io.twitter.repository.UserRepository;
import pl.polsl.io.twitter.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.polsl.io.twitter.test.UserBuilder.aUser;

public class SubscriptionServiceTest extends IntegrationTest {

	@Autowired
	SubscriptionService subscriptionService;

	@Test
	@UserStory("As a user I can subscribe to other users")
	@UserStory("As a user I can be subscribed by other users")
	public void shouldSubscribeToAnotherUser() {
		// given
		User subscribingUser = aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository
				(userRepository);
		User subscribedUser = aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository
				(userRepository);

		// when
		subscriptionService.subscribe(subscribingUser.getId(), subscribedUser.getId());

		// then
		assertThat(subscribingUser.getSubscriptions()).contains(subscribedUser);
		assertThat(subscribedUser.getSubscribingUsers()).contains(subscribingUser);
	}

	@Test(expected = IllegalArgumentException.class)
	@UserStory("As a user I cannot subscribe to myself")
	public void shouldNotSubscribeToHimselfOrHerself() {
		// given
		User subscribingUser = aUser().withMinimalData().withRole(roleRepository.findUserRole()).savedInRepository
				(userRepository);
		User subscribedUser = subscribingUser;

		// when
		subscriptionService.subscribe(subscribingUser.getId(), subscribedUser.getId());

		// then
		// ... an exception is thrown
	}

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;
}